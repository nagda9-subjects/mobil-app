package hu.ppke.itk.tk;

public class Parallelogram extends Shape implements Polygon {
    protected double _side_a;
    protected double _side_b;
    protected double _delta;
    protected double _height_a;

    public double getSideA() { return this._side_a; }
    public double getSideB() { return this._side_b; }
    public double getAngle() { return this._delta;  }

    public boolean isEquilateral() { return this._side_a==this._side_b; }
    public boolean isRight()  { return this._delta == 90; }

    public Parallelogram(double a, double b, double angle) {
        this._side_a = a;
        this._side_b = b;
        this._delta = angle;
        this._height_a = b * Math.sin(this._delta * Math.PI / 180.0);
    }

    @Override
    public void print() {
        System.out.println("Parallelogram a, b, delta: "
                + this._side_a + ", "
                + this._side_b + ", "
                + this._delta);
    }

    @Override
    public double calculateArea() {
        return this._side_a * this._height_a;
    }

    @Override
    public double[] getEdges() {
        return new double[] {_side_a, _side_b, _side_a, _side_b};
    }
}
