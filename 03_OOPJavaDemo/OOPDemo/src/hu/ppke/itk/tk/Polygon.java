package hu.ppke.itk.tk;

public interface Polygon {
    double[] getEdges();
}
