package hu.ppke.itk.tk;

public abstract class Shape {
    public abstract double calculateArea();
    public abstract void print();
}
