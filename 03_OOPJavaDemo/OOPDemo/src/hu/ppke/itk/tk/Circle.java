package hu.ppke.itk.tk;

public class Circle extends Shape{
    public static final double PI = 3.1415926535;

    private double radius;

    public Circle(double r) {
        this.radius = r;
    }

    @Override
    public void print() {
        System.out.println("Circle, radius: " + this.radius);
    }

    @Override
    public double calculateArea() {
        return this.radius * this.radius * Circle.PI;
    }
}
