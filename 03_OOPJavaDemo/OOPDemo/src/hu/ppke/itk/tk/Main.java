package hu.ppke.itk.tk;

public class Main {

    public static void main(String[] args) {
        Rectangle r = new Rectangle(10, 30);
        Shape s = r;
        s.print();
        System.out.println(s.calculateArea());

        Polygon p = r;
        System.out.println(p.getEdges().length);
    }
}
