package hu.ppke.itk.tk;

public final class Rectangle extends Parallelogram {
    public Rectangle(double a, double b) {
        super(a, b, 90);
    }

    public void print() {
        System.out.println("Rectangle a, b: "
                + this._side_a + ", "
                + this._side_b);
    }

    @Override
    public boolean isRight() {
        return true;
    }
}
