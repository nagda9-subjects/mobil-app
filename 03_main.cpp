#include <iostream>
#include <math.h>
double PI = 3.1415926535;

class Shape {
public:
    virtual ~Shape() {};
    virtual void print() const = 0;
    virtual double calculateArea() const = 0;
};

class Circle : public Shape {
    double radius;
public:
    Circle(double r): radius(r) {};

    virtual void print() const override {
        std::cout << "Circle radius: " << radius << std::endl;
    }
    virtual double calculateArea() const override {
        return radius * radius * PI;
    }
};

class Parallelogram : public Shape {
protected:
    double _side_a;
    double _side_b;
    double _delta;
    double _height_a;
public:
    virtual double get_a_side() const final { return _side_a; }
    virtual double get_b_side() const final { return _side_b; }
    virtual double get_angle()  const { return _delta;  }
    virtual bool is_equilateral() const { return _side_a == _side_b; }
    virtual bool is_right() const { return _delta == 90; }

    Parallelogram(double a, double b, double angle): _side_a(a), _side_b(b), _delta(angle) {
        _height_a = b * std::sin(_delta*PI / 180.0);
    }

    virtual void print() const override {
        std::cout << "Parallelogram a, b, delta: " << _side_a << ", "
                << _side_b << ", "
                << _delta << std::endl;
    }

    virtual double calculateArea() const override {
        return _side_a * _height_a;
    }
};

class Rhombus : public virtual Parallelogram {
public:
    Rhombus(double a, double angle): Parallelogram(a, a, angle) {  };

    virtual void print() const override {
        std::cout << "Rhombus side, angle " << _side_a << ", " << _delta << std::endl;
    }

    virtual double calculateArea() const override {
        return _side_a * _side_a * std::sin(_delta*PI / 180.0);
    }

    virtual bool is_equilateral() const override final { return true; }
};

class Rectangle : public virtual Parallelogram {
public:
    Rectangle(double a, double b): Parallelogram(a, b, 90) { _height_a = b; };

    virtual void print() const override {
        std::cout << "Rectangle " << _side_a << ", " << _side_b << std::endl;
    }

    virtual bool is_right() const override final { return true; }
};

class Square: public Rectangle, public Rhombus {
public:
    Square(double a): Parallelogram(a, a, 90), Rhombus(a, 90), Rectangle(a,a) { };
    //Square(double a): Rectangle(a,a), Rhombus(a, 90) { };

    virtual void print() const override final {
        std::cout << "Square " << _side_a << std::endl;
    }

    virtual double calculateArea() const override final {
        return _side_a * _side_a;
    }
};


int main() {
    Parallelogram *s = new Square(10);
    std::cout << s->calculateArea() << std::endl;
    s->print();
    delete s;
    
    return 0;
}
